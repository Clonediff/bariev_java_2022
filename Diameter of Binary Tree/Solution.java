/*
    Diameter of Binary Tree:
    Given the root of a binary tree, return the length of the diameter of the tree.
    The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.
    The length of a path between two nodes is represented by the number of edges between them.
*/

import java.util.*;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */

class Solution {
    public int diameterOfBinaryTree(TreeNode root) {
        return diameterOfBinaryTree(root, new HashMap<TreeNode, Integer>(), new HashMap<TreeNode, Integer>());
    }

    public int diameterOfBinaryTree(TreeNode root,
                                    Map<TreeNode, Integer> heightsMap,
                                    Map<TreeNode, Integer> diametersMap) {
        if (diametersMap.containsKey(root)){
            return diametersMap.get(root);
        }

        if (root == null){
            diametersMap.put(root, 0);
            return 0;
        }

        int leftTreeHeight = findHeightOfBinaryTree(root.left, heightsMap);
        int rightTreeHeight = findHeightOfBinaryTree(root.right, heightsMap);

        int leftTreeDiameter = diameterOfBinaryTree(root.left, heightsMap, diametersMap);
        int rightTreeDiameter = diameterOfBinaryTree(root.right, heightsMap, diametersMap);

        int diameter = Math.max(leftTreeHeight + rightTreeHeight + 2,
                Math.max(leftTreeDiameter, rightTreeDiameter));
        diametersMap.put(root, diameter);

        return diameter;
    }

    public int findHeightOfBinaryTree(TreeNode root, Map<TreeNode, Integer> heightsMap){
        if (heightsMap.containsKey(root)){
            return heightsMap.get(root);
        }

        int height = -1;
        if (root != null) {
            height = Math.max(findHeightOfBinaryTree(root.left, heightsMap),
                    findHeightOfBinaryTree(root.right, heightsMap)) + 1;
        }
        heightsMap.put(root, height);

        return height;
    }
}
