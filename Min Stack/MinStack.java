/*
    Min Stack:
    Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

    Implement the MinStack class:
        * MinStack() initializes the stack object.
        * void push(int val) pushes the element val onto the stack.
        * void pop() removes the element on the top of the stack.
        * int top() gets the top element of the stack.
        * int getMin() retrieves the minimum element in the stack.

    You must implement a solution with O(1) time complexity for each function.
 */

class MinStack {
    private class StackNode{
        int val;
        StackNode prev;
        StackNode next;
        int minInStack;

        public StackNode(int val){
            this.val = val;
        }
    }

    StackNode head;
    StackNode tail;

    public boolean isEmpty(){
        return head == null;
    }

    public void push(int val) {
        StackNode newNode = new StackNode(val);
        if (isEmpty()){
            newNode.minInStack = val;
            head = tail = newNode;
        } else{
            newNode.minInStack = Math.min(val, tail.minInStack);
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode;
        }
    }

    public void pop() {
        if (tail.prev != null) {
            tail.prev.next = null;
            tail = tail.prev;
        } else{
            head = tail = null;
        }
    }

    public int top() {
        return tail.val;
    }

    public int getMin() {
        return tail.minInStack;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
