/*
    Permutations:
    Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.
 */

import java.util.*;

class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        permute(nums, nums.length, result);
        return result;
    }

    public void permute(int[] nums, int leftToSwap, List<List<Integer>> result){
        if (leftToSwap == 1){
            List<Integer> temp = new ArrayList<Integer>();
            for (int i = 0; i < nums.length; i++){
                temp.add(nums[i]);
            }
            result.add(temp);
            return;
        }
        for (int i = 0; i < leftToSwap - 1; i++){
            permute(nums, leftToSwap - 1, result);
            if (leftToSwap % 2 == 0){
                swap(nums, i, leftToSwap - 1);
            } else {
                swap(nums, 0, leftToSwap - 1);
            }
        }
        permute(nums, leftToSwap - 1, result);
    }

    public  void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
