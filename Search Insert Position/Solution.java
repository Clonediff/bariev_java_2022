/*
    Search Insert Position:
    Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
    You must write an algorithm with O(log n) runtime complexity.
 */

class Solution {
    public int searchInsert(int[] nums, int target) {
        int l = -1;
        int r = nums.length;
        while (l != r - 1){
            int mid = (l + r) / 2;
            if (nums[mid] < target){
                l = mid;
            } else{
                r = mid;
            }
        }
        return r;
    }
}
