/*
    Remove Nth Node From End of List:
    Given the head of a linked list, remove the nth node from the end of the list and return its head.
 */

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        int currentIndex = 0;
        ListNode nthNodePrev = head;
        ListNode currentNode = head;
        while (currentNode.next != null){
            if (currentIndex >= n){
                nthNodePrev = nthNodePrev.next;
            }
            currentNode = currentNode.next;
            currentIndex++;
        }

        if (currentIndex == n - 1){
            return head.next;
        }
        if (currentIndex < n - 1){
            return head;
        }
        nthNodePrev.next = nthNodePrev.next.next;

        return head;
    }
}
