/*
    Rotate Array:
    Given an array, rotate the array to the right by k steps, where k is non-negative.
 */

class Solution {
    public void rotate(int[] nums, int k) {
        int startIndex = 0;
        int previous = nums[startIndex];
        int curIndex = k % nums.length;
        for (int setPositions = 0; setPositions < nums.length; setPositions++){
            int temp = nums[curIndex];
            nums[curIndex] = previous;
            previous = temp;

            if (curIndex == startIndex){
                startIndex = (startIndex + 1) % nums.length;
                previous = nums[startIndex];
                curIndex = (startIndex + k) % nums.length;
            } else {
                curIndex = (curIndex + k) % nums.length;
            }
        }
    }
}
