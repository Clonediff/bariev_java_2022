/*
    Remove Duplicates from Sorted List:
    Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.
 */

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null){
            return null;
        }

        ListNode lastNode = head;
        for (ListNode curNode = head.next, prevNode = head; curNode != null; curNode = curNode.next, prevNode = prevNode.next){
            if (curNode.val != prevNode.val){
                lastNode.next = curNode;
                lastNode = curNode;
            }
        }
        lastNode.next = null;

        return head;
    }
}
