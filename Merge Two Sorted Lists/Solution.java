/*
    Merge Two Sorted Lists:
    Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.
    Return the head of the merged linked list.
 */

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null) {
            return list2;
        }
        if (list2 == null){
            return list1;
        }

        ListNode head = new ListNode(Math.min(list1.val, list2.val));
        ListNode lastNode = head;

        ListNode list1CurNode = list1;
        ListNode list2CurNode = list2;
        if (head.val == list1.val) {
            list1CurNode = list1.next;
        }
        else {
            list2CurNode = list2.next;
        }

        while (list1CurNode != null && list2CurNode != null){
            if (list1CurNode.val < list2CurNode.val){
                lastNode.next = list1CurNode;
                list1CurNode = list1CurNode.next;
            } else {
                lastNode.next = list2CurNode;
                list2CurNode = list2CurNode.next;
            }
            lastNode = lastNode.next;
        }

        if (list1CurNode != null){
            lastNode.next = list1CurNode;
        }
        if (list2CurNode != null){
            lastNode.next = list2CurNode;
        }

        return head;
    }
}
