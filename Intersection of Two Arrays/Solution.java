/*
    Intersection of Two Arrays II:
    Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must appear as many times as it shows in both arrays and you may return the result in any order.
 */

import java.util.*;

class Solution {
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int[] cross = new int[Math.min(nums1.length, nums2.length)];
        int arr1Pointer = 0;
        int arr2Pointer = 0;
        int count = 0;
        while(arr1Pointer < nums1.length && arr2Pointer < nums2.length)
        {
            if (nums1[arr1Pointer] == nums2[arr2Pointer])
            {
                cross[count] = nums1[arr1Pointer];
                arr1Pointer++;
                arr2Pointer++;
                count++;
            }
            else if (nums1[arr1Pointer] < nums2[arr2Pointer])
                arr1Pointer++;
            else
                arr2Pointer++;
        }

        int[] result = new int[count];
        for (int i = 0; i < count; i++){
            result[i] = cross[i];
        }

        return result;
    }
}
