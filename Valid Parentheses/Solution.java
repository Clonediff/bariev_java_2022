/*
    Valid Parentheses:
    Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

    An input string is valid if:
        1. Open brackets must be closed by the same type of brackets.
        2. Open brackets must be closed in the correct order.
 */

import java.util.*;

class Solution {
    public boolean isValid(String s) {
        Stack<Character> stackOfParentheses = new Stack<Character>();

        char[] sAsCharArray = s.toCharArray();
        for (int i = 0; i < sAsCharArray.length; i++){
            if (sAsCharArray[i] == '{' || sAsCharArray[i] == '(' || sAsCharArray[i] == '['){
                stackOfParentheses.push(sAsCharArray[i]);
            } else {
                if (stackOfParentheses.size() == 0){
                    return false;
                }
                char lastParenthesis = stackOfParentheses.pop();
                switch (sAsCharArray[i]){
                    case '}':
                        if (lastParenthesis != '{'){
                            return false;
                        }
                        break;
                    case ')':
                        if (lastParenthesis != '('){
                            return false;
                        }
                        break;
                    case ']':
                        if (lastParenthesis != '['){
                            return false;
                        }
                        break;
                }
            }
        }
        return stackOfParentheses.size() == 0;
    }
}
